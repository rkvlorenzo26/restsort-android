package com.dev.projectrestsort;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dev.projectrestsort.adapters.RatesListAdapter;
import com.dev.projectrestsort.dialogs.LoadingDialog;
import com.dev.projectrestsort.entity.Rates;
import com.dev.projectrestsort.entity.Resort;
import com.dev.projectrestsort.utils.Constants;
import com.dev.projectrestsort.utils.Host;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AvailabilityActivity extends AppCompatActivity {

    List<Rates> ratesList = new ArrayList<>();
    RecyclerView rvAvailability;
    RatesListAdapter ratesListAdapter;

    private ProgressDialog mProgress;
    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_availability);

        initializeUI();
    }

    private void initializeUI() {
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button

        builder = new AlertDialog.Builder(this);

        mProgress = new LoadingDialog(this).generateDialog(Constants.LOADING_RESOURCES);
        mProgress.show();

        rvAvailability = findViewById(R.id.rvAvailabilityList);
        ratesListAdapter = new RatesListAdapter(this, ratesList);
        rvAvailability.setLayoutManager(new LinearLayoutManager(this));
        rvAvailability.setAdapter(ratesListAdapter);

        loadRates();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rates, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.rateSortByName) {
            List<Rates> tempList = new ArrayList<>();
            tempList.addAll(ratesList);

            Collections.sort(tempList, new Comparator<Rates>() {
                @Override
                public int compare(Rates lhs, Rates rhs) {
                    return lhs.getName().compareTo(rhs.getName());
                }
            });

            ratesList.clear();
            ratesList.addAll(tempList);
            ratesListAdapter.notifyDataSetChanged();
            return true;
        }

        if (id == R.id.rateSortByPriceDesc) {
            List<Rates> tempList = new ArrayList<>();
            tempList.addAll(ratesList);

            Collections.sort(tempList, new Comparator<Rates>() {
                @Override
                public int compare(Rates lhs, Rates rhs) {
                    return rhs.getPrice().compareTo(lhs.getPrice());
                }
            });

            ratesList.clear();
            ratesList.addAll(tempList);
            ratesListAdapter.notifyDataSetChanged();
            return true;
        }

        if (id == R.id.rateSortByPriceAsc) {
            List<Rates> tempList = new ArrayList<>();
            tempList.addAll(ratesList);

            Collections.sort(tempList, new Comparator<Rates>() {
                @Override
                public int compare(Rates lhs, Rates rhs) {
                    return lhs.getPrice().compareTo(rhs.getPrice());
                }
            });

            ratesList.clear();
            ratesList.addAll(tempList);
            ratesListAdapter.notifyDataSetChanged();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void loadRates() {
        String REQUEST_URL = new Host().getBaseURL() + "/load_rates.php";
        StringRequest stringReq = new StringRequest(Request.Method.POST, REQUEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject obj = new JSONObject(response);
                    String status = obj.getString("status");
                    if (status.equals("success")) {
                        JSONArray jsonArray = (JSONArray) obj.get("data");
                        ratesList.clear();
                        for (int x = 0; x < jsonArray.length(); x++) {
                            JSONObject data = new JSONObject(jsonArray.get(x).toString());
                            ratesList.add(new Rates(data.getString("rate_id"), data.getString("name"),
                                    data.getString("description"), data.getString("price"), data.getString("image"), data.getString("status")));
                        }
                        ratesListAdapter.notifyDataSetChanged();
                    } else {
                        builder.setMessage(obj.getString("message")).setNegativeButton("Okay", null).create().show();
                    }
                    mProgress.dismiss();
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                builder.setMessage(Constants.UNABLE_TO_CONNECT).setNegativeButton(Constants.OKAY, null).create().show();
                mProgress.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("resort_id", getIntent().getStringExtra("resort_id"));
                return params;
            }

        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringReq);
    }
}
