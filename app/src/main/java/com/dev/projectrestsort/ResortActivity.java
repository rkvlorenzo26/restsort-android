package com.dev.projectrestsort;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dev.projectrestsort.adapters.ResortSliderAdapter;
import com.dev.projectrestsort.dialogs.LoadingDialog;
import com.dev.projectrestsort.entity.Gallery;
import com.dev.projectrestsort.utils.Constants;
import com.dev.projectrestsort.utils.Host;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResortActivity extends AppCompatActivity {

    private TextView resortTitle, resortLocation, resortDescription;
    private Button btnAvailability, btnReviews;
    private SliderView sliderView;
    private ResortSliderAdapter resortSliderAdapter;
    private List<Gallery> galleryList = new ArrayList<>();

    private ProgressDialog mProgress;
    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resort);
        initializeUI();
    }

    private void initializeUI() {
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button

        builder = new AlertDialog.Builder(this);

        LinearLayout activityLayout = (LinearLayout) findViewById(R.id.activity_resort);
        activityLayout.setVisibility(View.GONE);

        resortTitle = findViewById(R.id.resortTitle);
        resortLocation = findViewById(R.id.resortLocation);
        resortDescription = findViewById(R.id.resortDescription);

        btnAvailability = findViewById(R.id.btnAvailability);
        btnReviews = findViewById(R.id.btnReviews);

        sliderView = findViewById(R.id.imageSlider);
        resortSliderAdapter = new ResortSliderAdapter(this, galleryList);
        sliderView.setSliderAdapter(resortSliderAdapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(5); //set scroll delay in seconds :
        sliderView.startAutoCycle();

        loadDetails();
        activityLayout.setVisibility(View.VISIBLE);
    }

    public void goToReviews(View view) {
        Intent intent = new Intent(ResortActivity.this, ReviewsActivity.class);
        intent.putExtra("resort_id", getIntent().getStringExtra("resort_id"));
        startActivity(intent);
    }

    public void goToAvailable(View view) {
        Intent intent = new Intent(ResortActivity.this, AvailabilityActivity.class);
        intent.putExtra("resort_id", getIntent().getStringExtra("resort_id"));
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void loadDetails() {
        mProgress = new LoadingDialog(this).generateDialog(Constants.LOADING_RESOURCES);
        mProgress.show();

        String REQUEST_URL = new Host().getBaseURL() + "/load_details.php";
        StringRequest stringReq = new StringRequest(Request.Method.POST, REQUEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject obj = new JSONObject(response);
                    String status = obj.getString("status");
                    if (status.equals("success")) {
                        JSONObject info = new JSONObject(obj.getString("info"));
                        resortTitle.setText(info.getString("name"));
                        resortLocation.setText(info.getString("location") + ", " + info.getString("city_municipality"));
                        resortDescription.setText(info.getString("description"));

                        JSONArray galleryArray = (JSONArray) obj.get("gallery");
                        for (int x = 0; x < galleryArray.length(); x++) {
                            JSONObject gallery = new JSONObject(galleryArray.get(x).toString());
                            galleryList.add(new Gallery(gallery.getString("title"), gallery.getString("image")));
                        }
                        resortSliderAdapter.notifyDataSetChanged();
                        mProgress.dismiss();
                    } else {
                        mProgress.dismiss();
                        builder.setMessage(obj.getString("message")).setNegativeButton("Okay", null).create().show();
                    }
                }catch (JSONException e){
                    mProgress.dismiss();
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                builder.setMessage(Constants.UNABLE_TO_CONNECT).setNegativeButton(Constants.OKAY, null).create().show();
                mProgress.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("resort_id", getIntent().getStringExtra("resort_id"));
                return params;
            }

        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringReq);
    }
}
