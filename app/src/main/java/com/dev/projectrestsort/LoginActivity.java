package com.dev.projectrestsort;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dev.projectrestsort.dialogs.LoadingDialog;
import com.dev.projectrestsort.entity.RegistrationForm;
import com.dev.projectrestsort.utils.Constants;
import com.dev.projectrestsort.utils.Host;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    EditText editTextEmail, editTextPassword;
    Button btnLogin, btnRegister;
    AlertDialog alertDialog;

    private FusedLocationProviderClient fusedLocationClient;
    private LocationRequest locationRequest;
    private ProgressDialog mProgress;
    private AlertDialog.Builder builder;

    private Double latitude = 0.0;
    private Double longitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initializeUI();
        getLocation();
    }

    private void initializeUI() {
        builder = new AlertDialog.Builder(this);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);
    }

    public void getLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    1000);
        }

        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    saveLocation(location);
                }
            }
        });
    }

    private void saveLocation (Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    public void login(View view) {
        String email = editTextEmail.getText().toString();
        String password = editTextPassword.getText().toString();

        if (email.trim().isEmpty() || password.trim().isEmpty()) {
            builder.setMessage(Constants.FORM_FILL_UP_FIELDS).setNegativeButton(Constants.OKAY, null).create().show();
        } else {
            if (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches() == false) {
                builder.setMessage(Constants.FORM_INV_EMAIL).setNegativeButton(Constants.OKAY, null).create().show();
            } else {
                executeLogin(email, password);
            }
        }
    }

    public void register(View view) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_register, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setTitle("Registration Form");
        alertDialog = dialogBuilder.create();

        Button btnDialogRegister = dialogView.findViewById(R.id.btnRegisterDialog);
        Button btnClose = dialogView.findViewById(R.id.btnCloseDialog);
        final EditText editTextFName = dialogView.findViewById(R.id.editTextFName);
        final EditText editTextLName = dialogView.findViewById(R.id.editTextLName);
        final EditText editTextContact = dialogView.findViewById(R.id.editTextPhone);
        final EditText editTextEmailReg = dialogView.findViewById(R.id.editTextEmailReg);
        final EditText editTextPasswordReg = dialogView.findViewById(R.id.editTextPasswordReg);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.hide();
            }
        });

        btnDialogRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstname = editTextFName.getText().toString();
                String lastname = editTextLName.getText().toString();
                String contact = editTextContact.getText().toString();
                String email = editTextEmailReg.getText().toString();
                String password = editTextPasswordReg.getText().toString();

                if (firstname.trim().isEmpty() || lastname.trim().isEmpty() || contact.trim().isEmpty() || email.trim().isEmpty() || password.trim().isEmpty()) {
                    builder.setMessage(Constants.FORM_FILL_UP_FIELDS).setNegativeButton(Constants.OKAY, null).create().show();
                } else {
                    if (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches() == false) {
                        builder.setMessage(Constants.FORM_INV_EMAIL).setNegativeButton(Constants.OKAY, null).create().show();
                    } else if (password.trim().length() < 8) {
                        builder.setMessage(Constants.FORM_INV_PASSWORD).setNegativeButton(Constants.OKAY, null).create().show();
                    } else if (contact.trim().length() != 11) {
                        builder.setMessage(Constants.FORM_INV_CONTACT).setNegativeButton(Constants.OKAY, null).create().show();
                    }  else {
                        executeRegistration(new RegistrationForm(firstname, lastname, contact, email, password));
                    }
                }
            }
        });
        alertDialog.show();
    }

    private void executeLogin(final String email, final String password) {
        mProgress = new LoadingDialog(this).generateDialog(Constants.VERIFYING_ACCOUNT);
        mProgress.show();

        String REQUEST_URL = new Host().getBaseURL() + "/login.php";
        StringRequest stringReq = new StringRequest(Request.Method.POST, REQUEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject obj = new JSONObject(response);
                    String status = obj.getString("status");
                    if (status.equals("success")) {
                        JSONObject data = new JSONObject(obj.getString("data"));

                        //Save Shared Preferences
                        SharedPreferences.Editor editor = getSharedPreferences(Constants.APP_SHARED_PREF, MODE_PRIVATE).edit();
                        editor.putString("user_id", data.getString("user_id"));
                        editor.putString("firstname", data.getString("firstname"));
                        editor.putString("lastname", data.getString("lastname"));
                        editor.putString("contact", data.getString("contact"));
                        editor.putString("email", data.getString("email"));
                        editor.putString("date_registered", data.getString("date_registered"));
                        editor.commit();

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                        mProgress.dismiss();
                    } else {
                        builder.setMessage(obj.getString("message")).setNegativeButton(Constants.OKAY, null).create().show();
                        mProgress.dismiss();
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                builder.setMessage(Constants.UNABLE_TO_CONNECT).setNegativeButton(Constants.OKAY, null).create().show();
                mProgress.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }

        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringReq);
    }

    private void executeRegistration(final RegistrationForm form) {
        mProgress = new LoadingDialog(this).generateDialog(Constants.REGISTERING_ACCOUNT);
        mProgress.show();

        String REQUEST_URL = new Host().getBaseURL() + "/registration.php";
        StringRequest stringReq = new StringRequest(Request.Method.POST, REQUEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject obj = new JSONObject(response);
                    String status = obj.getString("status");
                    if (status.equals("success")) {
                        builder.setMessage(obj.getString("message")).setNegativeButton("Okay", null).create().show();
                        alertDialog.hide();
                        mProgress.dismiss();
                    } else {
                        builder.setMessage(obj.getString("message")).setNegativeButton("Okay", null).create().show();
                        mProgress.dismiss();
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                builder.setMessage(Constants.UNABLE_TO_CONNECT).setNegativeButton(Constants.OKAY, null).create().show();
                mProgress.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("firstname", form.getFirstname());
                params.put("lastname", form.getLastname());
                params.put("contact", form.getContact());
                params.put("email", form.getEmail());
                params.put("password", form.getPassword());
                return params;
            }

        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringReq);
    }
}
