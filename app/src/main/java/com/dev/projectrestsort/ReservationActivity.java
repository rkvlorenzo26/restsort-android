package com.dev.projectrestsort;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dev.projectrestsort.adapters.ReservationListAdapter;
import com.dev.projectrestsort.adapters.ReviewListAdapter;
import com.dev.projectrestsort.dialogs.LoadingDialog;
import com.dev.projectrestsort.entity.Reservation;
import com.dev.projectrestsort.utils.Constants;
import com.dev.projectrestsort.utils.Host;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReservationActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<Reservation> reservationList = new ArrayList<>();
    ReservationListAdapter reservationListAdapter;

    private ProgressDialog mProgress;
    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);
        initializeUI();
    }

    private void initializeUI() {
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button

        builder = new AlertDialog.Builder(this);

        recyclerView = findViewById(R.id.rvReservationList);
        reservationListAdapter = new ReservationListAdapter(this, reservationList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(reservationListAdapter);
        loadReservations();
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void loadReservations() {
        mProgress = new LoadingDialog(this).generateDialog(Constants.LOADING_RESOURCES);
        mProgress.show();

        SharedPreferences prefs = this.getSharedPreferences(Constants.APP_SHARED_PREF, MODE_PRIVATE);
        final String user_id = prefs.getString("user_id", "");

        String REQUEST_URL = new Host().getBaseURL() + "/load_reservations.php";
        StringRequest stringReq = new StringRequest(Request.Method.POST, REQUEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject obj = new JSONObject(response);
                    String status = obj.getString("status");
                    if (status.equals("success")) {
                        JSONArray jsonArray = (JSONArray) obj.get("data");
                        reservationList.clear();
                        for (int x = 0; x < jsonArray.length(); x++) {
                            JSONObject data = new JSONObject(jsonArray.get(x).toString());
                            reservationList.add(
                                        new Reservation(
                                                    data.getString("resortName"),
                                                    data.getString("fromDate"),
                                                    data.getString("toDate"),
                                                    data.getString("instruction"),
                                                    data.getString("status")
                                                )
                                        );
                        }
                        reservationListAdapter.notifyDataSetChanged();
                    } else {
                        builder.setMessage(obj.getString("message")).setNegativeButton("Okay", null).create().show();
                    }
                    mProgress.dismiss();
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                builder.setMessage(Constants.UNABLE_TO_CONNECT).setNegativeButton(Constants.OKAY, null).create().show();
                mProgress.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                return params;
            }

        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringReq);
    }
}
