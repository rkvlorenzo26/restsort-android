package com.dev.projectrestsort;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dev.projectrestsort.adapters.ReviewListAdapter;
import com.dev.projectrestsort.dialogs.LoadingDialog;
import com.dev.projectrestsort.entity.Review;
import com.dev.projectrestsort.utils.Constants;
import com.dev.projectrestsort.utils.Host;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReviewsActivity extends AppCompatActivity {

    List<Review> reviewList = new ArrayList<>();
    RecyclerView recyclerView;
    ReviewListAdapter reviewListAdapter;

    private ProgressDialog mProgress;
    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;

    EditText editTextReview;
    Button btnAddReviewDialog, btnCloseReviewDialog;
    ImageButton btnAddImg;
    TextView imagePath;
    RatingBar reviewRating;

    private String filePath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);
        initializeUI();
    }

    private void initializeUI() {
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button

        builder = new AlertDialog.Builder(this);

        recyclerView = findViewById(R.id.rvReviewList);
        reviewListAdapter = new ReviewListAdapter(this, reviewList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(reviewListAdapter);

        loadReviews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.add_review) {
            showReviewFrom();

        }

        return super.onOptionsItemSelected(item);
    }

    private void showReviewFrom() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_review, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setTitle("Add Review");
        alertDialog = dialogBuilder.create();

        editTextReview = dialogView.findViewById(R.id.editTextReview);
        btnAddImg = dialogView.findViewById(R.id.btnAddImg);
        btnAddReviewDialog = dialogView.findViewById(R.id.btnAddReviewDialog);
        btnCloseReviewDialog = dialogView.findViewById(R.id.btnCloseReviewDialog);
        imagePath = dialogView.findViewById(R.id.imagePath);
        reviewRating = dialogView.findViewById(R.id.reviewRating);

        btnAddImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 1);
            }
        });

        btnAddReviewDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String review = editTextReview.getText().toString();
            int rating = (int) reviewRating.getRating();
            if (review.trim().isEmpty()) {
                builder.setMessage(Constants.FORM_FILL_UP_FIELDS).setNegativeButton(Constants.OKAY, null).create().show();
            } else {
                addReview(review, rating, filePath);
                alertDialog.hide();
            }

            filePath = null;
            imagePath.setText("");
            }
        });

        btnCloseReviewDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.hide();
            }
        });

        alertDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(uri,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            filePath = picturePath;
            imagePath.setText("Path:" + filePath);
        }
    }

    private void addReview(final String review, final int rating, String filePath) {
        mProgress = new LoadingDialog(this).generateDialog(Constants.ADDING_REVIEW);
        mProgress.show();

        String REQUEST_URL = new Host().getBaseURL() + "/add_review.php";

        SharedPreferences prefs = this.getSharedPreferences(Constants.APP_SHARED_PREF, MODE_PRIVATE);
        final String user_id = prefs.getString("user_id", "");
        final String resort_id = getIntent().getStringExtra("resort_id");
        final String imgPath = filePath;

        StringRequest stringReq = new StringRequest(Request.Method.POST, REQUEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("REVIEW", response);
                try{
                    JSONObject obj = new JSONObject(response);
                    String status = obj.getString("status");
                    if (status.equals("success")) {
                        builder.setMessage(obj.getString("message")).setNegativeButton(Constants.OKAY, null).create().show();
                        mProgress.dismiss();
                        loadReviews();
                    } else {
                        builder.setMessage(obj.getString("message")).setNegativeButton(Constants.OKAY, null).create().show();
                        mProgress.dismiss();
                    }
                }catch (JSONException e){
                    mProgress.dismiss();
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                builder.setMessage(Constants.UNABLE_TO_CONNECT).setNegativeButton(Constants.OKAY, null).create().show();
                mProgress.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String encodedImage = "";
                if(imgPath != null){
                    File imgFile = new File(imgPath);
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    encodedImage = (Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT));
                }

                Map<String, String> params = new HashMap<>();
                params.put("resort_id", resort_id);
                params.put("user_id", user_id);
                params.put("review", review);
                params.put("ratings", String.valueOf(rating));
                params.put("image", encodedImage);
                Log.v("REVIEW", params.toString());
                return params;
            }

        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringReq);
    }

    private void loadReviews() {
        mProgress = new LoadingDialog(this).generateDialog(Constants.LOADING_RESOURCES);
        mProgress.show();

        String REQUEST_URL = new Host().getBaseURL() + "/load_reviews.php";
        StringRequest stringReq = new StringRequest(Request.Method.POST, REQUEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject obj = new JSONObject(response);
                    String status = obj.getString("status");
                    if (status.equals("success")) {
                        JSONArray jsonArray = (JSONArray) obj.get("data");
                        reviewList.clear();
                        for (int x = 0; x < jsonArray.length(); x++) {
                            JSONObject data = new JSONObject(jsonArray.get(x).toString());
                            reviewList.add(
                                    new Review(data.getString("name"),
                                            data.getString("comments"),
                                            data.getString("ratings"),
                                            data.getString("image")));
                        }
                        reviewListAdapter.notifyDataSetChanged();
                    } else {
                        builder.setMessage(obj.getString("message")).setNegativeButton("Okay", null).create().show();
                    }
                    mProgress.dismiss();
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                builder.setMessage(Constants.UNABLE_TO_CONNECT).setNegativeButton(Constants.OKAY, null).create().show();
                mProgress.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("resort_id", getIntent().getStringExtra("resort_id"));
                return params;
            }

        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringReq);
    }
}
