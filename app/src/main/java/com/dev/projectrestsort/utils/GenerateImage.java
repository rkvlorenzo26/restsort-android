package com.dev.projectrestsort.utils;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

public class GenerateImage {
    public TextDrawable generate(String initials) {
        int color = ColorGenerator.MATERIAL.getColor(initials);
        return TextDrawable.builder()
                .buildRoundRect(initials, color, 10);
    }
}
