package com.dev.projectrestsort.utils;

public class Host {
//    private final String URL = "http://192.168.254.102/restsort/android";
//    private final String GALLERY_URL = "http://192.168.254.102/restsort/uploads/gallery/";
//    private final String RATES_URL = "http://192.168.254.102/restsort/uploads/rates/";
//    private final String REVIEWS_URL = "http://192.168.254.102/restsort/uploads/reviews/";

    //hosting
    private final String URL = "https://restsort2020.000webhostapp.com/android";
    private final String GALLERY_URL = "https://restsort2020.000webhostapp.com/uploads/gallery/";
    private final String RATES_URL = "http://restsort2020.000webhostapp.com/uploads/rates/";
    private final String REVIEWS_URL = "http://restsort2020.000webhostapp.com/uploads/reviews/";

    public String getBaseURL() {
        return URL;
    }

    public String getGALLERY_URL() {
        return GALLERY_URL;
    }

    public String getRATES_URL() {
        return RATES_URL;
    }

    public String getREVIEWS_URL() { return REVIEWS_URL; }
}
