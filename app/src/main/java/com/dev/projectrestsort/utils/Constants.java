package com.dev.projectrestsort.utils;

public class Constants {
    public static final String APP_SHARED_PREF = "RESTSORT";

    //Loading
    public static final String VERIFYING_ACCOUNT = "Verifying account. Please wait...";
    public static final String REGISTERING_ACCOUNT = "Registering account. Please wait...";
    public static final String ADDING_REVIEW = "Adding review. Please wait...";
    public static final String UNABLE_TO_CONNECT = "Unable to connect to the server.";
    public static final String LOADING_RESOURCES = "Loading resources. Please wait...";
    public static final String SENDING_RESERVATION = "Sending reservation request. Please wait...";


    //Forms
    public static final String FORM_FILL_UP_FIELDS = "Please fill up all fields.";
    public static final String FORM_INV_EMAIL = "Invalid email address.";
    public static final String FORM_INV_CONTACT = "Please enter valid contact no.";
    public static final String FORM_INV_PASSWORD = "Password must be 8 characters.";

    //Buttons
    public static final String OKAY = "Okay";
}
