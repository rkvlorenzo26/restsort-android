package com.dev.projectrestsort.entity;

public class Rates {
    private String rate_id;
    private String name;
    private String description;
    private String price;
    private String image;
    private String status;

    public Rates(String rate_id, String name, String description, String price, String image, String status) {
        this.rate_id = rate_id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.image = image;
        this.status = status;
    }

    public String getRate_id() {
        return rate_id;
    }

    public void setRate_id(String rate_id) {
        this.rate_id = rate_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setStatus(String status) { this.status = status; }

    public String getStatus() { return status; }
}
