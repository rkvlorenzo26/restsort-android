package com.dev.projectrestsort.entity;

public class Resort {
    private int resort_id;
    private String name;
    private String city_municipality;
    private String image;
    private String distance;
    private String ratings;

    public Resort() { }

    public Resort(int resort_id, String name, String city_municipality,
                  String image, String distance, String ratings) {
        this.resort_id = resort_id;
        this.name = name;
        this.city_municipality = city_municipality;
        this.image = image;
        this.distance = distance;
        this.ratings = ratings;
    }

    public int getResort_id() {
        return resort_id;
    }

    public void setResort_id(int resort_id) {
        this.resort_id = resort_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity_municipality() {
        return city_municipality;
    }

    public void setCity_municipality(String city_municipality) {
        this.city_municipality = city_municipality;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRatings() {
        return ratings;
    }

    public void setRatings(String ratings) {
        this.ratings = ratings;
    }

    @Override
    public String toString() {
        return "ResortActivity{" +
                "resort_id=" + resort_id +
                ", name='" + name + '\'' +
                ", city_municipality='" + city_municipality + '\'' +
                ", image='" + image + '\'' +
                ", distance='" + distance + '\'' +
                ", ratings='" + ratings + '\'' +
                '}';
    }
}
