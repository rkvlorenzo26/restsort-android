package com.dev.projectrestsort.entity;

public class Review {
    private String name;
    private String comments;
    private String ratings;
    private String image;

    public Review(String name, String comments, String ratings, String image) {
        this.name = name;
        this.comments = comments;
        this.ratings = ratings;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getRatings() {
        return ratings;
    }

    public void setRatings(String ratings) {
        this.ratings = ratings;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Review{" +
                "name='" + name + '\'' +
                ", comments='" + comments + '\'' +
                ", ratings='" + ratings + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
