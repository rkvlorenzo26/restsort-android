package com.dev.projectrestsort.entity;

public class Gallery {
    private String title;
    private String image;

    public Gallery(String title, String image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Gallery{" +
                "title='" + title + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
