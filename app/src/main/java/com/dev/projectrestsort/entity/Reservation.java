package com.dev.projectrestsort.entity;

public class Reservation {
    private String resortName;
    private String fromDate;
    private String toDate;
    private String instruction;
    private String status;

    public Reservation(String resortName, String fromDate, String toDate, String instruction, String status) {
        this.resortName = resortName;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.instruction = instruction;
        this.status = status;
    }

    public String getResortName() {
        return resortName;
    }

    public void setResortName(String resortName) {
        this.resortName = resortName;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
