package com.dev.projectrestsort.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.text.HtmlCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dev.projectrestsort.R;
import com.dev.projectrestsort.dialogs.LoadingDialog;
import com.dev.projectrestsort.entity.Rates;
import com.dev.projectrestsort.utils.Constants;
import com.dev.projectrestsort.utils.Host;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class RatesListAdapter extends RecyclerView.Adapter<RatesListAdapter.MyViewHolder>{
    private Context context;
    private List<Rates> ratesList;
    private ProgressDialog mProgress;
    private AlertDialog.Builder builder;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgView;
        public TextView name, description, price;
        public Button btnReserveNow;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.textAName);
            description = view.findViewById(R.id.textADescription);
            price = view.findViewById(R.id.textAPrice);
            imgView = view.findViewById(R.id.rvAImage);
            btnReserveNow = view.findViewById(R.id.btnReserveNow);
        }
    }

    public RatesListAdapter(Context context, List<Rates> ratesList) {
        this.context = context;
        this.ratesList = ratesList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_availability_new, viewGroup, false);
        return new RatesListAdapter.MyViewHolder(itemView);
    }

    private Boolean readTerms = false;

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final Rates rate = ratesList.get(i);
        myViewHolder.name.setText(rate.getName());
        myViewHolder.description.setText(rate.getDescription());
        myViewHolder.price.setText("Price: ₱" + rate.getPrice());

        final String URL = new Host().getRATES_URL() + rate.getImage();
        Picasso.with(context).load(URL).into(myViewHolder.imgView);

        if (rate.getStatus().equals("Available")) {
            myViewHolder.btnReserveNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                    LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.dialog_reservation, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setTitle("ReservationActivity Form");
                    final AlertDialog alertDialog = dialogBuilder.create();

                    readTerms = false;

                    Button btnReservation = dialogView.findViewById(R.id.btnReservation);
                    Button btnCloseReservation = dialogView.findViewById(R.id.btnCloseReservation);
                    ImageButton btnFromDate = dialogView.findViewById(R.id.btnFromDate);
                    ImageButton btnToDate = dialogView.findViewById(R.id.btnToDate);
                    final EditText reserveFromDate = dialogView.findViewById(R.id.reserveFromDate);
                    final EditText reserveToDate = dialogView.findViewById(R.id.reserveToDate);
                    final EditText reserveInstructions = dialogView.findViewById(R.id.reserveInstructions);
                    final TextView tvTermsAndConditions = dialogView.findViewById(R.id.tvTermsAndConditions);

                    tvTermsAndConditions.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog dialog = new Dialog(context); // Context, this, etc.
                            dialog.setContentView(R.layout.dialog_terms_and_conditions);

                            int width = (int)(context.getResources().getDisplayMetrics().widthPixels*0.90);
                            int height = (int)(context.getResources().getDisplayMetrics().heightPixels*0.90);

                            dialog.getWindow().setLayout(width, height);

                            TextView contentTitle = dialog.findViewById(R.id.tvContentTitle);
                            TextView content = dialog.findViewById(R.id.tvContent);
                            Button close = dialog.findViewById(R.id.btnCloseContent);

                            close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    readTerms = true;
                                    dialog.cancel();
                                }
                            });

                            String content_title = context.getResources().getString(R.string.terms_and_conditions_title);
                            String content_1 = "<p>" + context.getResources().getString(R.string.terms_and_conditions_1) + "</p>";
                            String content_2 = "<p><b>" + context.getResources().getString(R.string.terms_and_conditions_2) + "</b></p>";
                            String content_3 = "<p>" + context.getResources().getString(R.string.terms_and_conditions_3) + "</p>";
                            String content_4 = "<p><b>" + context.getResources().getString(R.string.terms_and_conditions_4) + "</b></p>";
                            String content_5 = "<p>" + context.getResources().getString(R.string.terms_and_conditions_5) + "</p>";
                            String content_6 = "<p>" + context.getResources().getString(R.string.terms_and_conditions_6) + "</p>";

                            contentTitle.setText(content_title);
                            String allContent = content_1 + "\n\n" + content_2 + "\n\n" + content_3 + "\n\n" + content_4 + "\n\n" + content_5 + "\n\n" + content_6;
                            content.setText(HtmlCompat.fromHtml(allContent, HtmlCompat.FROM_HTML_MODE_COMPACT));
                            dialog.show();
                        }
                    });

                    btnFromDate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Calendar c = Calendar.getInstance();
                            int mYear = c.get(Calendar.YEAR);
                            int mMonth = c.get(Calendar.MONTH);
                            int mDay = c.get(Calendar.DAY_OF_MONTH);
                            c.add(Calendar.DATE, 1);
                            DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                                    new DatePickerDialog.OnDateSetListener() {
                                        @Override
                                        public void onDateSet(DatePicker view, int year,
                                                              int monthOfYear, int dayOfMonth) {
                                            reserveFromDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                            reserveToDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                        }
                                    }, mYear, mMonth, mDay);
                            datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                            datePickerDialog.show();
                        }
                    });

                    btnToDate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Calendar c = Calendar.getInstance();
                            int mYear = c.get(Calendar.YEAR);
                            int mMonth = c.get(Calendar.MONTH);
                            int mDay = c.get(Calendar.DAY_OF_MONTH);
                            c.add(Calendar.DATE, 1);
                            DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                                    new DatePickerDialog.OnDateSetListener() {
                                        @Override
                                        public void onDateSet(DatePicker view, int year,
                                                              int monthOfYear, int dayOfMonth) {
                                            reserveToDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                        }
                                    }, mYear, mMonth, mDay);
                            datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                            datePickerDialog.show();
                        }
                    });

                    btnReservation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String dateFrom = reserveFromDate.getText().toString();
                            String dateTo = reserveToDate.getText().toString();
                            String instructions = reserveInstructions.getText().toString().trim();

                            if (dateFrom.isEmpty() || dateTo.isEmpty()) {
                                Toast.makeText(context, "Please fill-up required fields.", Toast.LENGTH_LONG).show();
                            } else if (!readTerms) {
                                Toast.makeText(context, "Please read terms and conditions..", Toast.LENGTH_LONG).show();
                            } else {
                                Calendar c1 = Calendar.getInstance();
                                Calendar c2 = Calendar.getInstance();
                                c1.setTime(convertToDate(dateFrom));
                                c2.setTime(convertToDate(dateTo));
                                int daysDiff = daysBetween(c1.getTime(), c2.getTime());

                                if (daysDiff < 0) {
                                    Toast.makeText(context, "Invalid ReservationActivity Date.", Toast.LENGTH_LONG).show();
                                } else {
                                    submitReservation(dateFrom, dateTo, instructions, rate.getRate_id());
                                    alertDialog.hide();
                                }
                            }
                        }
                    });

                    btnCloseReservation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.hide();
                        }
                    });
                    alertDialog.show();
                }
            });
        } else {
            myViewHolder.btnReserveNow.setBackgroundColor(ContextCompat.getColor(context, R.color.colorUnavailable));
            myViewHolder.btnReserveNow.setEnabled(false);
            myViewHolder.btnReserveNow.setText(rate.getStatus());
        }
    }

    private int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }

    private Date convertToDate(String dateString) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            Date date = sdf.parse(dateString);
            return date;
        } catch (Exception e) {
            e.printStackTrace();return null;
        }
    }

    private void submitReservation(final String dateFrom, final String dateTo, final String instructions, final String rate_id) {
        mProgress = new LoadingDialog(context).generateDialog(Constants.SENDING_RESERVATION);
        mProgress.show();
        builder = new AlertDialog.Builder(context);

        String REQUEST_URL = new Host().getBaseURL() + "/add_reservation.php";
        SharedPreferences prefs = context.getSharedPreferences(Constants.APP_SHARED_PREF, MODE_PRIVATE);
        final String user_id = prefs.getString("user_id", "");

        StringRequest stringReq = new StringRequest(Request.Method.POST, REQUEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject obj = new JSONObject(response);
                    String status = obj.getString("status");
                    if (status.equals("success")) {
                        builder.setMessage(obj.getString("message")).setNegativeButton(Constants.OKAY, null).create().show();
                        mProgress.dismiss();
                    } else {
                        builder.setMessage(obj.getString("message")).setNegativeButton(Constants.OKAY, null).create().show();
                        mProgress.dismiss();
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                    mProgress.dismiss();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                builder.setMessage(Constants.UNABLE_TO_CONNECT).setNegativeButton(Constants.OKAY, null).create().show();
                mProgress.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("rate_id", rate_id);
                params.put("reservation_date_from", dateFrom);
                params.put("reservation_date_to", dateTo);
                params.put("instructions", instructions);
                return params;
            }

        };

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringReq);
    }

    @Override
    public int getItemCount() { return ratesList.size(); }
}
