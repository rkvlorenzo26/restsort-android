package com.dev.projectrestsort.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.dev.projectrestsort.R;
import com.dev.projectrestsort.ResortActivity;
import com.dev.projectrestsort.entity.Resort;
import com.dev.projectrestsort.utils.Host;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ResortListAdapter extends RecyclerView.Adapter<ResortListAdapter.MyViewHolder> {
    private Context context;
    private List<Resort> resortList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgView;
        public TextView textName, textLocation, textRatings, textDistance;
        public RatingBar ratingBar;

        public MyViewHolder(View view) {
            super(view);
            imgView = view.findViewById(R.id.rvImage);
            textName = view.findViewById(R.id.textName);
            textLocation = view.findViewById(R.id.textLocation);
            textRatings = view.findViewById(R.id.textRatings);
            textDistance = view.findViewById(R.id.textDistance);
            ratingBar = view.findViewById(R.id.reviewRating);
        }
    }

    public ResortListAdapter(Context context, List<Resort> resortList) {
        this.context = context;
        this.resortList = resortList;
    }

    @NonNull
    @Override
    public ResortListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_resort_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ResortListAdapter.MyViewHolder myViewHolder, final int position) {
        final Resort resort = resortList.get(position);
        myViewHolder.textName.setText(resort.getName());
        myViewHolder.textLocation.setText(resort.getCity_municipality());
        myViewHolder.textRatings.setText("Ratings: ");
        myViewHolder.textDistance.setText("0 km away");
        myViewHolder.ratingBar.setRating(Float.parseFloat(resort.getRatings()));

        final String URL = new Host().getGALLERY_URL() + resort.getImage();
        Picasso.with(context).load(URL).into(myViewHolder.imgView);

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(context, ResortActivity.class);
            intent.putExtra("resort_id", String.valueOf(resort.getResort_id()));
            ((Activity) context).startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return resortList.size();
    }
}
