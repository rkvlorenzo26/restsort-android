package com.dev.projectrestsort.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.projectrestsort.R;
import com.dev.projectrestsort.entity.Reservation;

import java.util.List;

public class ReservationListAdapter extends RecyclerView.Adapter<ReservationListAdapter.MyViewHolder> {
    private Context context;
    private List<Reservation> reservationList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView reservationDate, reservationResort, reservationInstruction, reservationStatus;

        public MyViewHolder(View view) {
            super(view);
            reservationDate = view.findViewById(R.id.reservationDate);
            reservationResort = view.findViewById(R.id.reservationResort);
            reservationInstruction = view.findViewById(R.id.reservationInstruction);
            reservationStatus = view.findViewById(R.id.reservationStatus);
        }
    }

    public ReservationListAdapter(Context context, List<Reservation> reservationList) {
        this.context = context;
        this.reservationList = reservationList;
    }

    @NonNull
    @Override
    public ReservationListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_reservation, viewGroup, false);
        return new ReservationListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReservationListAdapter.MyViewHolder myViewHolder, int position) {
        Reservation reservation = reservationList.get(position);
        myViewHolder.reservationDate.setText("Date: " + reservation.getFromDate() + " to " + reservation.getToDate());
        myViewHolder.reservationResort.setText("Resort: " + reservation.getResortName());
        myViewHolder.reservationInstruction.setText("Instructions: " + reservation.getInstruction());
        myViewHolder.reservationStatus.setText("Status: " + reservation.getStatus());
    }

    @Override
    public int getItemCount() {
        return reservationList.size();
    }
}
