package com.dev.projectrestsort.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.projectrestsort.R;
import com.dev.projectrestsort.entity.Gallery;
import com.dev.projectrestsort.utils.Host;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ResortSliderAdapter extends SliderViewAdapter<ResortSliderAdapter.SliderAdapterVH> {

    private Context context;
    private List<Gallery> galleryList;

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            this.itemView = itemView;
        }
    }

    public ResortSliderAdapter(Context context, List<Gallery> galleryList) {
        this.context = context;
        this.galleryList = galleryList;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_resort_slider, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        viewHolder.textViewDescription.setText(galleryList.get(position).getTitle());
        final String URL = new Host().getGALLERY_URL() + galleryList.get(position).getImage();
        Picasso.with(context).load(URL).into(viewHolder.imageViewBackground);
    }

    @Override
    public int getCount() {
        return galleryList.size();
    }
}
