package com.dev.projectrestsort.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.dev.projectrestsort.R;
import com.dev.projectrestsort.entity.Review;
import com.dev.projectrestsort.utils.Host;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.MyViewHolder> {
    private Context context;
    private List<Review> reviewList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView reviewName, reviewComment;
        public RatingBar ratingBar;
        public ImageView reviewImage;

        public MyViewHolder(View view) {
            super(view);
            reviewName = view.findViewById(R.id.reviewName);
            reviewComment = view.findViewById(R.id.reviewComment);
            ratingBar = view.findViewById(R.id.reviewRatingBar);
            reviewImage = view.findViewById(R.id.reviewImage);
        }
    }

    public ReviewListAdapter(Context context, List<Review> reviewList) {
        this.context = context;
        this.reviewList = reviewList;
    }

    @NonNull
    @Override
    public ReviewListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_reviews, viewGroup, false);
        return new ReviewListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewListAdapter.MyViewHolder myViewHolder, int position) {
        Review review = reviewList.get(position);
        myViewHolder.reviewName.setText(review.getName());
        myViewHolder.reviewComment.setText(review.getComments());
        myViewHolder.ratingBar.setRating(Float.parseFloat(review.getRatings()));

        if (!review.getImage().isEmpty()) {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 0, 20);
            myViewHolder.reviewImage.setLayoutParams(lp);

            final String URL = new Host().getREVIEWS_URL() + review.getImage();
            Picasso.with(context).load(URL).into(myViewHolder.reviewImage);
        } else {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 0, 0);
            myViewHolder.reviewImage.setLayoutParams(lp);
            myViewHolder.reviewImage.setImageResource(0);
        }
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }
}
