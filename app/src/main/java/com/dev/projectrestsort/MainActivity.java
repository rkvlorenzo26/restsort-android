package com.dev.projectrestsort;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dev.projectrestsort.adapters.ResortListAdapter;
import com.dev.projectrestsort.dialogs.LoadingDialog;
import com.dev.projectrestsort.entity.Resort;
import com.dev.projectrestsort.utils.Constants;
import com.dev.projectrestsort.utils.GenerateImage;
import com.dev.projectrestsort.utils.Host;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    List<Resort> resortList = new ArrayList<>();
    NavigationView navigationView;
    RecyclerView recyclerView;
    ResortListAdapter resortListAdapter;

    private ProgressDialog mProgress;
    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        initializeUI();
    }

    private void initializeUI() {
        builder = new AlertDialog.Builder(this);

        mProgress = new LoadingDialog(this).generateDialog(Constants.LOADING_RESOURCES);
        mProgress.show();

        SharedPreferences prefs = this.getSharedPreferences(Constants.APP_SHARED_PREF, MODE_PRIVATE);

        View headerView = navigationView.getHeaderView(0);
        TextView drawerName = headerView.findViewById(R.id.drawerName);
        TextView drawerEmail = headerView.findViewById(R.id.drawerEmail);
        ImageView drawerImg = headerView.findViewById(R.id.drawerImage);

        drawerName.setText(prefs.getString("firstname", "") + " " +  prefs.getString("lastname", ""));
        drawerEmail.setText(prefs.getString("email", ""));

        String initials = prefs.getString("firstname", "").substring(0, 1) + prefs.getString("lastname", "").substring(0, 1);
        drawerImg.setImageDrawable(new GenerateImage().generate(initials));

        recyclerView = findViewById(R.id.rvResortList);
        resortListAdapter = new ResortListAdapter(this, resortList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(resortListAdapter);
        loadResorts();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.mainSortByName) {
            List<Resort> tempList = new ArrayList<>();
            tempList.addAll(resortList);

            Collections.sort(tempList, new Comparator<Resort>() {
                @Override
                public int compare(Resort lhs, Resort rhs) {
                    return lhs.getName().compareTo(rhs.getName());
                }
            });

            resortList.clear();
            resortList.addAll(tempList);
            resortListAdapter.notifyDataSetChanged();
            return true;
        }

        if (id == R.id.mainSortByRatings) {
            List<Resort> tempList = new ArrayList<>();
            tempList.addAll(resortList);

            Collections.sort(tempList, new Comparator<Resort>() {
                @Override
                public int compare(Resort lhs, Resort rhs) {
                    return rhs.getRatings().compareTo(lhs.getRatings());
                }
            });

            resortList.clear();
            resortList.addAll(tempList);
            resortListAdapter.notifyDataSetChanged();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            finish();
            startActivity(intent);
        }

        if (id == R.id.nav_reservation) {
            Intent intent = new Intent(MainActivity.this, ReservationActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadResorts() {
        String REQUEST_URL = new Host().getBaseURL() + "/load_resorts.php";
        StringRequest stringReq = new StringRequest(Request.Method.POST, REQUEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject obj = new JSONObject(response);
                    String status = obj.getString("status");
                    if (status.equals("success")) {
                       JSONArray jsonArray = (JSONArray) obj.get("data");
                       resortList.clear();
                        for (int x = 0; x < jsonArray.length(); x++) {
                            JSONObject data = new JSONObject(jsonArray.get(x).toString());
                            resortList.add(new Resort(data.getInt("resort_id"), data.getString("name"),
                                    data.getString("city_municipality"), data.getString("image"), "", data.getString("ratings")));
                        }
                        resortListAdapter.notifyDataSetChanged();
                    } else {
                        builder.setMessage(obj.getString("message")).setNegativeButton("Okay", null).create().show();
                    }
                    mProgress.dismiss();
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                builder.setMessage(Constants.UNABLE_TO_CONNECT).setNegativeButton(Constants.OKAY, null).create().show();
                mProgress.dismiss();
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringReq);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            loadResorts();
        }
    }
}
