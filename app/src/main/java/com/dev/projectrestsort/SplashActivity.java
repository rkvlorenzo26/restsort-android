package com.dev.projectrestsort;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.dev.projectrestsort.dialogs.GpsDialogs;
import com.dev.projectrestsort.utils.LocationHelper;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    private boolean checkLocationPermission = false;
    private boolean checkStoragePermission = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    1000);
        } else {
            checkLocationPermission = true;
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,},
                    2000);
        } else {
            checkStoragePermission = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1000: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkLocationPermission = true;
                } else {
                    Toast.makeText(getBaseContext(), "Please allow location permission in app settings.", Toast.LENGTH_LONG).show();
                    finish();
                }
                return;
            }
            case 2000: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkStoragePermission = true;
                } else {
                    Toast.makeText(getBaseContext(), "Please allow storage permission in app settings.", Toast.LENGTH_LONG).show();
                    finish();
                }
                return;
            }
        }
    }

    public void statusCheck() {
        boolean isOnline = new LocationHelper(this).statusCheck();

        if (isOnline == false) {
            new GpsDialogs(this).generateGPSDialog();
        } else {
           launchLoginForm();
        }
    }

    private void launchLoginForm() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkLocationPermission && checkStoragePermission) {
            statusCheck();
        }
    }
}
